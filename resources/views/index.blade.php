@extends('layouts.app')

@section('content')
<h1>Todos</h1>

    @if(count($todos)> 0)
        @foreach($todos as $todo)
        <div class="well m-2">
            <a href="todo/{{$todo->id}}"><h3>{{$todo->title}}</h3></a>
            <p>{{$todo->content}}</p>
            <span class="label label-danger">{{$todo->due}}</span>
        </div>
        @endforeach
    @endif    
@endsection
