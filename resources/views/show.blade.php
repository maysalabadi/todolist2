@extends('layouts.app')

@section('content')
<a href="/" class="btn btn-default">Go Back</a>
<h1>Todo</h1>
          
        <div class="well m-2">
            <h3>{{$todo->title}}</h3>
            <span class="label label-danger">{{$todo->due}}</span>
            <hr>
            <p>{{$todo->content}}</p>
           
        </div>       
        <div>
             <form method="post" action="/todo/{{$todo->id}}">
            @csrf
            @method('DELETE')
                <button type="submit" class="btn btn-danger pull-right">Delete</button>
            </form>
            <a href="/todo/{{ $todo->id }}/edit" class="btn btn-primary">Edit</a>      
        </div>
     
@endsection
