<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ToDo List</title>
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="{{asset('/css/_custom.css') }}"> 
    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" />
       
</head>
<body>
     @include('inc.navbar')
     @include('inc.messages')
    <div class="container">
        @yield('content')
    </div>
    <footer class="text-center">
        Copyright 2019
    </footer>
</body>
</html>